import React, {Component} from 'react';
import {connect} from "react-redux";
import {BrowserRouter, Route} from "react-router-dom";
import {Navbar} from "./components/Navbar";
import AddUser from "./components/AddUser";
import {Home} from "./components/Home";
import Dashboard from "./components/Dashboard";

class App extends Component {

    render() {
        return (
            <BrowserRouter>
                <div className="main-app">
                    <Navbar/>
                    <Route path='/users' component={Dashboard}/>
                    <Route path='/game' component={AddUser}/>
                    <Route exact path='/' component={Home}/>
                </div>
            </BrowserRouter>
        );
    }
}

export default connect()(App);
