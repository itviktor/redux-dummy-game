export function setUsername(name) {
    return {
        type: 'SET_USERNAME',
        payload: {
            name,
            step: 2
        }
    }
}

export function setUserAction(action) {
    return {
        type: 'SET_USER_ACTION',
        payload: {
            action,
            step: 3
        }
    }
}

export function refreshForm() {
    return {
        type: 'RESTART_GAME_FORM'
    }
}

export function setUserDate(date) {
    return {
        type: 'SET_USER_DATE',
        payload: {
            date,
            step: 4
        }
    }
}

export function setUserLocation(location) {
    return {
        type: 'SET_USER_LOCATION',
        payload: {
            location,
            step: 5
        }
    }
}

export function EvaluateUser(user) {
    return {
        type: 'EVALUATE_USER',
        payload: user
    }
}

export const setUserLocationAndDispatch = (userstring) => dispatch => Promise.resolve(dispatch(setUserLocation(userstring)));
