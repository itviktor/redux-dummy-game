const initState = {
    step: 1,
    userForm: {
        name: '',
        action: '',
        date: '',
        location: '',
    }
};
export default function userFormReducer(state = initState, action) {
    switch (action.type) {
        case "RESTART_GAME_FORM":
            return initState;
        case "SET_USERNAME":
            return {
                ...state,
                step: action.payload.step,
                userForm: {...state.userForm, name: action.payload.name}
            };
        case "SET_USER_ACTION":
            return {
                ...state,
                step: action.payload.step,
                userForm: {...state.userForm, action: action.payload.action}
            };
        case "SET_USER_DATE":
            return {
                ...state,
                step: action.payload.step,
                userForm: {...state.userForm, date: action.payload.date}
            };
        case "SET_USER_LOCATION":
            return {
                ...state,
                step: action.payload.step,
                userForm: {...state.userForm, location: action.payload.location}
            };

        default:
            return state;
    }
}