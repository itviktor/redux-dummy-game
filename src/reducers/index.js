import {combineReducers} from "redux";
import userFormReducer from "./userFormReducer";
import userReducer from "./userReducer";

export const allReducers = combineReducers({
    userForm: userFormReducer,
    users: userReducer
});