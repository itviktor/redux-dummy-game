import {NavLink} from "react-router-dom";
import React from 'react'
import PropTypes from "prop-types";

export const GameResult = ({resultString, handleFormRefresh}) => (
    <div className="game-container flex-direction-column">
        <h4>{resultString}</h4>
        <br/>
        <button onClick={handleFormRefresh} className="btn-large btn">Try it again</button>
        <p>OR</p>
        <NavLink to="/users">Check your previous results</NavLink>
    </div>
);
GameResult.propTypes = {
        resultString: PropTypes.string.isRequired,
        handleFormRefresh: PropTypes.func.isRequired
};