import React from 'react';
import {NavLink} from "react-router-dom";

export const Navbar = () => {
    return (
        <nav className="nav-wrapper red darken-3">
            <div className="container">
                <span className="brand-logo show-on-large">Dummy Game</span>
                <ul className="right">
                    <li><NavLink to="/">Home page</NavLink></li>
                    <li><NavLink to="/game">Game</NavLink></li>
                    <li><NavLink to="/users">Dashboard</NavLink></li>
                </ul>
            </div>
        </nav>
    );
};