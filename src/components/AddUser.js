import {
    EvaluateUser,
    refreshForm,
    setUserAction,
    setUserDate,
    setUserLocationAndDispatch,
    setUsername
} from "../actions/userFormActions";
import {connect} from "react-redux";
import React, {Component} from 'react'
import GenericInputForm from "./GenericInput";
import {GameResult} from "./GameResult";

class AddUser extends Component {

    handleFormRefresh() {
        this.props.dispatch(refreshForm());
    }

    submitName(value) {
        this.props.dispatch(setUsername(value));
    }

    buildResultString() {
        return `${this.props.userData.name} ${this.props.userData.action} ${this.props.userData.location} ${this.props.userData.date}`;
    }

    submitLocation(value) {
        /*
        hope no one will notice that line, I guess here should be smth called thunk or saga
        But I don't know how to use that, yet:D
         */
        this.props.dispatch(setUserLocationAndDispatch(value)).then(() => {
            this.props.dispatch(EvaluateUser(this.buildResultString()));
        });
    }

    submitAction(value) {
        this.props.dispatch(setUserAction(value));
    }

    submitDate(value) {
        this.props.dispatch(setUserDate(value));
    }

    displayInput() {
        switch (this.props.step) {
            case 1:
                return (
                    <GenericInputForm submitInput={this.submitName.bind(this)}
                                      title={'Who ?'}
                                      buttonTitle={'Next'}/>
                );
            case 2:
                return (
                    <GenericInputForm submitInput={this.submitAction.bind(this)}
                                      title={`What?`}
                                      buttonTitle={'Next'}/>
                );
            case 3:
                return (
                    <GenericInputForm submitInput={this.submitDate.bind(this)}
                                      title={`When ?`}
                                      buttonTitle={'Next'}/>
                );
            case 4:
                return (
                    <GenericInputForm submitInput={this.submitLocation.bind(this)}
                                      title={'Where?'}
                                      buttonTitle={'Finish'}/>
                );
            case 5:
                return (<GameResult resultString={this.buildResultString()}
                                    handleFormRefresh={this.handleFormRefresh.bind(this)}
                />);
            default:
                return (
                    <GenericInputForm submitInput={this.submitName.bind(this)}
                                      title={'What is your name'}
                                      buttonTitle={'Next'}/>
                );

        }
    }

    render() {
        return (
            <div className="row container">
                {this.displayInput()}
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {step: state.userForm.step, userData: state.userForm.userForm}
};
export default connect(mapStateToProps)(AddUser);