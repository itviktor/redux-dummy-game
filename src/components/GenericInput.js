import React, {Component} from 'react'
import PropTypes from 'prop-types';

export default class GenericInputForm extends Component {
    constructor() {
        super();
        this.state = {
            value: '',
            error: false
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        const value = event.target[0].value;
        if (value.length >= 1) {
            this.setState({error: false, value: ''});
            this.props.submitInput(value);
        } else {
            this.setState({error: true});
        }
    }

    handleChange(event) {
        const value = event.target.value;
        if (value.length < 1) {
            this.setState({error: true, value})
        } else {
            this.setState({error: false, value})
        }
    }

    render() {
        return (
            <div className="game-container">
                <form className="col s9" onSubmit={this.handleSubmit.bind(this)}>
                    <div className="row">
                        <div className="input-field col s9">
                            <h4>{this.props.title}</h4>
                            <input onChange={this.handleChange.bind(this)} type="text" value={this.state.value}
                                   className={this.state.error ? 'invalid' : ''}/>
                            {this.state.error ? <span className="red-text">Field is required!</span> : null}
                        </div>
                        <div className="input-field col s3">
                            <button type="submit" className="btn-large btn">{this.props.buttonTitle}</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}
GenericInputForm.propTypes = {
    buttonTitle: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    submitInput: PropTypes.func.isRequired
};