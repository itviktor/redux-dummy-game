import React from 'react';
import {NavLink} from "react-router-dom";

export const Home = () => {
    return (<div className="game-container">
        <NavLink to="/game">
            <button className="btn btn-large">Start game</button>
        </NavLink>
    </div>);
};