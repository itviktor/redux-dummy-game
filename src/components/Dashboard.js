import {connect} from "react-redux";
import React, {Component} from 'react'
import PropTypes from "prop-types";

class Dashboard extends Component {
    render() {
        return (
            <div className="container">
                <h4 className="center">Dashboard: </h4>
                {this.props.users.length ? this.props.users.map((user, index) => (
                    <p key={index}>{user}</p>)) : 'No results yet...'}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {users: state.users}
};
export default connect(mapStateToProps)(Dashboard);
Dashboard.propTypes = {
    users: PropTypes.array
};